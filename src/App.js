import logo from "./logo.svg";
import "./App.css";
import { Button } from "@ui-toolkit/xr-button";
import "@ui-toolkit/xr-button/dist/index.css";
import { Line } from "@ui-toolkit/xr-chart";
import { Sandbox } from "./sandbox-builder";
import { PROJECT_NAMES } from "./projectIDs.constants";

const SCALE = {
  width: "600",
  height: "400",
};

const LineData = {
  labels: ["Red", "Blue", "Yellow", "Green", "Purple", "Orange"],
  datasets: [
    {
      label: "# of Votes",
      data: [12, 19, 3, 5, 2, 3],
      fill: true,
    },
  ],
};

const LineOptions = {
  responsive: false,
};

const lineProps = {
  onclick: { action: "onclick" },
  id: "LINE_CHART",
  data: LineData,
  ...SCALE,
  options: {
    ...LineOptions,
    plugins: {
      legend: {
        position: "top",
      },
      title: {
        display: true,
        text: "Line Chart",
      },
    },
  },
};

function App() {
  return (
    <div className="App">
      {/* <Button size="small" label="Button" />
      <br />
      <Button type="secondary" label="Button" />

      <Line {...lineProps} /> */}

      <iframe
        is="x-frame-bypass"
        // src="https://gitlab.com/-/ide/project/yashsavimunavat/checkbox/edit/master/-/@output=embed"
        src={`https://atggitlab-srv.xoriant.com/-/ide/project/ui-toolkit/react-ui/xr-button/edit/develop/-/`}
        // title={`${platform}-component`}
        title="web ide"
        style={{
          margin: "3px",
          width: "100%",
          height: "350px",
          border: "1px solid black",
          borderRadius: "4px",
          overflow: "hidden",
        }}
        allow="accelerometer; ambient-light-sensor; camera; encrypted-media; geolocation; gyroscope; hid; microphone; midi; payment; usb; vr; xr-spatial-tracking"
      ></iframe>
      <Sandbox
        platform="react"
        component={PROJECT_NAMES.XORIANT_REACT_BUTTON}
        pathToOpen="src%2Findex.js"
      />
    </div>
  );
}

export default App;
