import { Gitlab } from "@gitbeaker/browser";
import { PROJECT_IDS } from "./projectIDs.constants";
// import { IGNORE_FILE_LIST } from "./ingnoreFileList.constants";

const ref = "master";

const gapi = new Gitlab({
  host: "https://gitlab.com",
  token: process.env.REACT_APP_GITLAB_TOKEN,
});

/**
 * Fetch raw file from the Gitlab Repository using Gitlab api.
 * @param {string} projectId - Project ID of the Repository.
 * @param {string} path - Path of the specified file.
 * @returns {Promise} - Raw file in string format
 */
const getRawFile = async (projectId, path) => {
  return gapi.RepositoryFiles.showRaw(projectId, path, { ref: ref });
};

/**
 * Fetch Repository tree that contains all the files with their details.
 * @param {string} projectId - Project ID of the Repository.
 * @returns {Promise} - Array of objects where each object
 *                      corresponds to individual file.
 */
const getRepoTree = async (projectId) => {
  return gapi.Repositories.tree(projectId, { recursive: true, ref: ref });
};

/**
 * Retrieve only the files that are required.
 * @param {string} projectId - Project ID of the Repository.
 * @returns {Promise} - Array of strings of required filenames.
 */
const getFiles = async (projectId) => {
  const filesObj = await getRepoTree(projectId);
  const filesArray = [...filesObj];
  const filenames = filesArray
    .filter((file) => (file.type === "tree" ? false : true))
    .map((file) => file.path);
  const excludeFiles = [
    // ...IGNORE_FILE_LIST,
    ".gitignore",
    ".npmignore",
    "babel.conf.json",
    "package-lock.json",
    "public/logo192.png",
    "public/logo512.png",
    "public/favicon.ico",
    "src/favicon.ico",
  ];
  const requiredFiles = filenames.filter(
    (file) => !excludeFiles.includes(file)
  );
  console.log("files", requiredFiles);
  return requiredFiles;
};

/**
 * Builds the object that is required by codesandbox api.
 * @param {string} platform - React or Angular.
 * @returns {Promise} - Object containing all the files
 *                      and their raw content.
 * @see {@link https://codesandbox.io/docs/api/#define-api}
 */
export const getParameters = async (platform, component) => {
  const projectId = PROJECT_IDS[component];
  const paramFiles = await getFiles(projectId);
  let parameters = { files: {} };

  await Promise.all(
    paramFiles.map(async (filename) => {
      return (parameters.files[filename] = {
        content: await getRawFile(projectId, filename),
      });
    })
  );
  return parameters;
};
