import React, { useCallback, useEffect, useState } from "react";
import { getParameters } from "./parameter-builder";

export const Sandbox = ({ platform, component, pathToOpen }) => {
  const [sandboxId, setSandboxId] = useState("");
  const [error, setError] = useState(null);
  const [loading, setLoading] = useState(false);

  const fetchSandboxId = useCallback(async () => {
    try {
      setLoading(true);
      const params = await getParameters(platform, component);
      const response = await fetch(
        "https://codesandbox.io/api/v1/sandboxes/define?json=1",
        {
          method: "POST",
          headers: {
            "Content-Type": "application/json",
            Accept: "application/json",
          },
          body: JSON.stringify(params),
        }
      );
      const data = await response.json();
      setSandboxId(data.sandbox_id);
      setLoading(false);
    } catch (e) {
      setLoading(false);
      setError("Something went wrong!");
    }
  }, [platform, component]);

  useEffect(() => {
    fetchSandboxId();
  }, [fetchSandboxId]);

  return (
    <>
      {loading && (
        <p style={{ textAlign: "center" }}>Loading {platform} codesandbox...</p>
      )}
      {!!sandboxId && !error && (
        <>
          <iframe
            src={`https://codesandbox.io/embed/${sandboxId}?fontsize=12&view=split&hidenavigation=1&module=${pathToOpen}&theme=dark`}
            title={`${platform}-component`}
            style={{
              margin: "3px",
              width: "100%",
              height: "350px",
              border: "1px solid black",
              borderRadius: "4px",
              overflow: "hidden",
            }}
            allow="accelerometer; ambient-light-sensor; camera; encrypted-media; geolocation; gyroscope; hid; microphone; midi; payment; usb; vr; xr-spatial-tracking"
            sandbox="allow-forms allow-modals allow-popups allow-presentation allow-same-origin allow-scripts"
          ></iframe>
        </>
      )}
      {error && <p>{error}</p>}
    </>
  );
};
